# Ubuntu22p04_FixBuiltInVncCanNotLink

## RealVNC Viewer

https://www.realvnc.com/en/connect/download/viewer/

1. Open the "Properties" (2 methods)

    1. Right click on the title bar

        ![]() <img src="img/RealVncViewer01.A.png"  width="800">

    2. Modify the profile in RealVNC Viewer main windows

        ![]() <img src="img/RealVncViewer01.B.png"  width="600">

2. Change the connection quality to high in REALVNC viewer

    ![]() <img src="img/RealVncViewer02.png"  width="600">

